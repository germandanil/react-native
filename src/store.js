import {createStore, compose, applyMiddleware} from 'redux';
import rootReducer from './reducer/reducer';
import createSagaMiddleware from 'redux-saga';
import {sagaWatcher} from './saga/sagas';
const saga = createSagaMiddleware();
const store = createStore(rootReducer, compose(applyMiddleware(saga)));

saga.run(sagaWatcher);
export default store;
