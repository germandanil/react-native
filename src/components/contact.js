import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

export default ({navigation, contact}) => {
  let user = contact.item;
  const number = user.phoneNumbers[0]?.number;
  const name = user.givenName;
  const avatar = user.thumbnailPath;
  const email = user.emailAddresses[0]?.email;
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('Profile contact', {...user});
      }}>
      <View style={styles.container}>
        <Image
          style={styles.tinyLogo}
          source={avatar ? {uri: avatar} : require('../icons/img.png')}
        />
        <View style={styles.infoBlank}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.number}>{number}</Text>
          {email ? <Text style={styles.email}>{email}</Text> : false}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
    flexDirection: 'row',
    backgroundColor: '#fff',
    alignItems: 'center',
    margin: 10,
  },
  name: {
    lineHeight: 24,
    fontSize: 24,
  },
  number: {
    lineHeight: 24,
    fontSize: 18,
  },
  email: {
    lineHeight: 20,
    fontSize: 16,
  },
  tinyLogo: {
    width: 80,
    height: 80,
  },
  infoBlank: {
    marginLeft: 10,
  },
});

