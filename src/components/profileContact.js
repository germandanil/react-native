import React, {useMemo, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faTrash, faPhoneAlt} from '@fortawesome/free-solid-svg-icons';
import {useDispatch} from 'react-redux';
import {deleteContact, updateContact} from '../reducer/actions';

import {Linking} from 'react-native';
export default ({route, navigation: {goBack}}) => {
  const trashIcon = <FontAwesomeIcon icon={faTrash} size={26} color={'#400'} />;
  const phoneIcon = (
    <FontAwesomeIcon icon={faPhoneAlt} size={26} color={'#051'} />
  );
  const contact = route.params;
  const recordID = contact.recordID;
  const startNumber = contact.phoneNumbers[0]?.number;
  const startName = contact.givenName;
  const avatar = contact.thumbnailPath;
  const email = contact.emailAddresses[0]?.email;
  const dispatch = useDispatch();
  const [number, setNumber] = useState(startNumber);
  const [name, setName] = useState(startName);
  const satisfactorily = useMemo(() => {
    return isSatisfactorily();
  }, [name, number]);
  function changeToDevice() {
    if (satisfactorily) {
      dispatch(
        updateContact.request(
          {
          ...contact,
          phoneNumbers: [
            {
              label: 'mobile',
              number: number.toString(),
            },
          ],
          givenName: name,
        }),
      );
      goBack();
    }
  }
  function onDelete() {
    dispatch(deleteContact.request(contact));
    goBack();
  }
  function isSatisfactorily() {
    if (number?.length > 1 && name.length > 1)
      if (startNumber !== number || name !== startName) return true;
      else return false;
  }
  return (
    <View style={styles.container} >
      <View style={styles.avatarConteiner}>
        <Image
          style={styles.logo}
          source={avatar ? {uri: avatar} : require('../icons/img.png')}
        />
      </View>
      <Text style={styles.title}>Name</Text>
      <TextInput
        style={styles.inputField}
        onChangeText={setName}
        value={name}
      />
      <Text style={styles.title}>Number</Text>
      <TextInput
        keyboardType={'number-pad'}
        style={styles.inputField}
        onChangeText={setNumber}
        value={number}
      />
      <View style={styles.editContainer}>
        <TouchableOpacity
          style={satisfactorily ? styles.editButton : styles.buttonDisabled}
          onPress={changeToDevice}>
          <Text style={satisfactorily ? styles.editText : styles.textDisabled}>
            Edit
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.toolContainer}>
        <TouchableHighlight
          onPress={() => Linking.openURL(`tel:${startNumber}`)}>
          <View style={styles.phoneButton}>{phoneIcon}</View>
        </TouchableHighlight>
        <TouchableHighlight onPress={onDelete}>
          <View style={styles.deleteButton}>{trashIcon}</View>
        </TouchableHighlight>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  avatarConteiner: {
    alignItems: 'center',
    marginVertical: 15,
  },
  inputField: {
    color: 'black',
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingVertical: 8,
    marginVertical: 4,
    marginHorizontal: 10,
    marginBottom: 20,
    fontSize: 20,
    textAlign: 'center',
  },
  title: {
    fontSize: 26,
    textAlign: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#bbb',
  },
  buttonDisabled: {
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#015',
    paddingHorizontal: 40,
    paddingVertical: 15,
    borderRadius: 6,
  },
  textDisabled: {
    color: '#013',
    fontSize: 20,
  },
  logo: {
    width: 150,
    height: 150,
  },
  disabled: {
    backgroundColor: '#111',
    color: '#222',
  },
  deleteButton: {
    color: '#520',
    alignItems: 'center',
    backgroundColor: '#e66',
    paddingVertical: 15,
    paddingHorizontal: 40,
    borderRadius: 6,
  },
  phoneButton: {
    paddingVertical: 15,
    paddingHorizontal: 40,
    alignItems: 'center',
    backgroundColor: '#6e8',
    borderRadius: 6,
  },
  toolContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    marginBottom: 20,
  },
  editContainer: {
    flex: 1,
    alignItems: 'center',
  },
  editButton: {
    alignItems: 'center',
    backgroundColor: '#48f',
    paddingHorizontal: 30,
    paddingVertical: 15,
    borderRadius: 6,
  },
  editText: {
    fontSize: 22,
    color: 'white',
  },
});
