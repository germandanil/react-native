import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faUserPlus} from '@fortawesome/free-solid-svg-icons';
import {StyleSheet, View, TouchableHighlight} from 'react-native';
export default function ({navigation}) {
  const AddIcon = (
    <FontAwesomeIcon icon={faUserPlus} size={26} color={'#222'} />
  );
  return (
    <TouchableHighlight
      onPress={() => {
        navigation.navigate('Add contact');
      }}>
      <View style={styles.button}>{AddIcon}</View>
    </TouchableHighlight>
  );
}
const styles = StyleSheet.create({
  button: {
    backgroundColor: '#dff',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
});
