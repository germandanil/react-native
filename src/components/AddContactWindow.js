import React, {useMemo, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {addContact} from '../reducer/actions';
export default function ({navigation: {goBack}}) {
  const dispatch = useDispatch();
  const [number, setNumber] = useState('');
  const [name, setName] = useState('');
  const satisfactorily = useMemo(() => {
    return isSatisfactorily();
  }, [name, number]);
  function addToDevice() {
    if (satisfactorily) {
      dispatch(
        addContact.request({
          phoneNumbers: [
            {
              label: 'mobile',
              number: number.toString(),
            },
          ],
          givenName: name,
        }),
      );
      goBack();
    }
  }
  function isSatisfactorily() {
    if (number.length > 1 && name.length > 1) return true;
    else return false;
  }
  return (
    <View style={styles.container}>
      <View style={styles.avatarConteiner}>
        <Image style={styles.logo} source={require('../icons/img.png')} />
      </View>
      <Text style={styles.title}>Name</Text>
      <TextInput style={styles.inputField} onChangeText={setName} />
      <Text style={styles.title}>Number</Text>
      <TextInput
        keyboardType={'number-pad'}
        style={styles.inputField}
        onChangeText={setNumber}
      />
      <View style={styles.editContainer}>
        <TouchableOpacity
          style={satisfactorily ? styles.editButton : styles.buttonDisabled}
          onPress={addToDevice}>
          <Text style={satisfactorily ? styles.editText : styles.textDisabled}>
            Add
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  avatarConteiner: {
    alignItems: 'center',
    marginVertical: 15,
    marginBottom: 40,
  },
  inputField: {
    color: 'black',
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingVertical: 8,
    marginVertical: 4,
    marginHorizontal: 10,
    marginBottom: 20,
    fontSize: 20,
  },
  title: {
    fontSize: 26,
    textAlign: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#bbb',
  },
  logo: {
    width: 150,
    height: 150,
  },
  buttonDisabled: {
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#015',
    paddingHorizontal: 40,
    paddingVertical: 15,
    borderRadius: 6,
  },
  textDisabled: {
    color: '#013',
    fontSize: 20,
  },
  editContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop: 40,
  },
  editButton: {
    alignItems: 'center',
    backgroundColor: 'blue',
    paddingHorizontal: 40,
    paddingVertical: 15,
    borderRadius: 6,
  },
  editText: {
    fontSize: 22,
    color: 'white',
  },
});
