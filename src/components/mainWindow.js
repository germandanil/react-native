import React from 'react';
import {StyleSheet, View} from 'react-native';
import Search from './Search';
import {FlatList} from 'react-native-gesture-handler';
import {fetchAllContacts} from '../reducer/actions';
import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import Contact from './Contact';
import AddContact from './AddContact';

export default ({navigation}) => {
  const contacts = useSelector(state => state.contacts);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchAllContacts.request());
  },[]);
  return (
    <View style={styles.container}>
      <Search />
      <AddContact navigation={navigation} />
      <FlatList
        data={contacts}
        renderItem={item => <Contact navigation={navigation} contact={item} />}
        keyExtractor={item => item.recordID}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#bbb',
  },
});
