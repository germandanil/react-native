import React from 'react';
import {useState} from 'react';
import {SearchBar} from 'react-native-elements';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faSearch, faTimes} from '@fortawesome/free-solid-svg-icons';
import {useDispatch, useSelector} from 'react-redux';

import {fetchSearchContacts} from '../reducer/actions';

export default function Search() {
  const dispatch = useDispatch();
  const [state, setState] = useState('');
  const SearchIcon = <FontAwesomeIcon onPress={onSearch} icon={faSearch} />;
  const Close = <FontAwesomeIcon onPress={onClear} icon={faTimes} />;
  updateSearch = search => {
    setState(search);
  };
  const contacts = useSelector(state => state.contacts);
  function onClear() {
    setState('');
  }
  function onSearch() {
    dispatch(fetchSearchContacts.request(state));
    onClear();
  }
  return (
    <SearchBar
      placeholder="Type Here..."
      onChangeText={updateSearch}
      value={state}
      lightTheme={true}
      onSubmitEditing={onSearch}
      searchIcon={SearchIcon}
      clearIcon={Close}
      onClear={onClear}
    />
  );
}
