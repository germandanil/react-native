import React from 'react';

import {takeEvery, put, call} from '@redux-saga/core/effects';
import 'react-native-get-random-values';
import {
  updateContact,
  addContact,
  fetchSearchContacts,
  fetchAllContacts,
  deleteContact,
} from '../reducer/actions';

import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';
export function* sagaWatcher() {
  yield takeEvery(fetchAllContacts.REQUEST, sagaFetchAllContacts);
  yield takeEvery(fetchSearchContacts.REQUEST, sagaFetchSearchContacts);
  yield takeEvery(addContact.REQUEST, sagaAddContact);
  yield takeEvery(deleteContact.REQUEST, sagaDeleteContact);
  yield takeEvery(updateContact.REQUEST, sagaUpdateContact);
}

function* sagaAddContact(action) {
  try {
    const contact = action.payload;
    if (contact.phoneNumbers && contact.givenName) {
      const user = yield call(requestAddContact, contact);
      if (user) yield put(addContact.success({contact: user}));
    }
  } catch (err) {}
}
function* sagaUpdateContact(action) {
  try {
    const contact = action.payload;
    if (contact.phoneNumbers && contact.givenName) {
      const user = yield call(requestUpdateContact, contact);
      if (user) yield put(updateContact.success(contact));
    }
  } catch (err) {
    console.log(err);
  }
}
function* sagaDeleteContact(action) {
  try {
    const contact = action.payload;
    const recordID = yield call(requestDeleteContact, contact);
    if (!recordID) yield put(addContact.failure({contact}));
  } catch (err) {
    console.log(err);
  }
}
function* sagaFetchAllContacts() {
  const contacts = yield call(requestAll);
  const payload = {contacts: contacts};
  yield put(fetchAllContacts.success(payload));
}
function* sagaFetchSearchContacts(action) {
  const contacts = yield call(requestAll);
  const payload = {contacts: contacts};
  yield put(fetchSearchContacts.success({...payload, search: action.payload}));
}
async function requestAddContact(contact) {
  if (await requestWriteContactPermission()) {
    return await Contacts.addContact(contact);
  } else return {};
}
async function requestUpdateContact(contact) {
  if (await requestWriteContactPermission()) {
    return await Contacts.updateContact(contact);
  } else return {};
}
async function requestDeleteContact(contact) {
  if (await requestWriteContactPermission()) {
    return await Contacts.deleteContact(contact);
  } else return {};
}
async function requestAll() {
  if (await requestReadContactPermission())
    return await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
      {
        title: 'Contacts',
        message: 'This app would like to view your contacts.',
        buttonPositive: 'Please accept bare mortal',
      },
    ).then(() => {
      return Contacts.getAll()
        .then(contacts => {
          return contacts;
        })
        .catch(e => {
          console.log(e);
        });
    });
  else return [];
}

async function requestReadContactPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
      {
        title: 'Read Contacts Permission',
        message: 'You should accept permission for use this app',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.warn(err);
  }
}

async function requestWriteContactPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
      {
        title: 'Write Contacts Permission',
        message: 'You should accept permission for use this app',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.warn(err);
    return false;
  }
}
