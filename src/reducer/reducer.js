import {
  fetchAllContacts,
  fetchSearchContacts,
  addContact,
  deleteContact,
  updateContact,
} from './actions';
const initialState = {
  contacts: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case addContact.SUCCESS: {
      const contact = action.payload.contact;
      const newContacts = [...state.contacts];
      newContacts.push(contact);
      return {
        ...state,
        contacts: newContacts,
      };
    }
    case fetchAllContacts.SUCCESS: {
      const contacts = action.payload.contacts;
      return {
        ...state,
        contacts,
      };
    }
    case updateContact.SUCCESS: {
      const contacts = state.contacts.filter(contact => {
        return contact.recordID !== action.payload.recordID;
      });
      contacts.push(action.payload);
      return {
        ...state,
        contacts,
      };
    }
    case fetchSearchContacts.SUCCESS: {
      const contacts = action.payload.contacts.filter(contact => {
        return contact.givenName.includes(action.payload.search);
      });
      return {
        ...state,
        contacts,
      };
    }
    case deleteContact.REQUEST: {
      const contacts = state.contacts.filter(contact => {
        return contact.recordID !== action.payload.recordID;
      });
      return {
        ...state,
        contacts,
      };
    }
    case deleteContact.FAILURE: {
      const contacts = [...state.contacts];
      contacts.push(action.payload.contact);
      return {
        ...state,
        contacts,
      };
    }
    default:
      return state;
  }
}
