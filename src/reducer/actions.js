import {
  CHANGE_CONTACT,
  ADD_CONTACT,
  FETCH_SEARCH_CONTACTS,
  FETCH_ALL_CONTACTS,
  DELETE_CONTACT,
} from './actionTypes';
import {createRoutine} from 'redux-saga-routines';

export const updateContact = createRoutine(CHANGE_CONTACT);
export const addContact = createRoutine(ADD_CONTACT);
export const fetchSearchContacts = createRoutine(FETCH_SEARCH_CONTACTS);
export const fetchAllContacts = createRoutine(FETCH_ALL_CONTACTS);
export const deleteContact = createRoutine(DELETE_CONTACT);
