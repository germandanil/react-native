export const FETCH_ALL_CONTACTS = 'FETCH_ALL_CONTACTS';
export const FETCH_SEARCH_CONTACTS = 'FETCH_SEARCH_CONTACTS';
export const ADD_CONTACT = 'ADD_CONTACT';
export const CHANGE_CONTACT = 'CHANGE_CONTACT';
export const DELETE_CONTACT = 'DELETE_CONTACT';
