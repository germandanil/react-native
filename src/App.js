import * as React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import MainWindow from './components/MainWindow';
import AddContactWindow from './components/AddContactWindow';
import ProfileContact from './components/ProfileContact';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Contacts" >
        <Stack.Screen name="Contacts" component={MainWindow} />
        <Stack.Screen name="Add contact" component={AddContactWindow} />
        <Stack.Screen name="Profile contact" component={ProfileContact} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
